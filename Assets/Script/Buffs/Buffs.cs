﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buffs : MonoBehaviour
{
    public int type;

    private void Start()
    {
        int rndm = Random.Range(1, 4);
        type = rndm;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            FindObjectOfType<BuffManager>().ShowBuffType(type);
            Destroy(this.gameObject);
        }

        if (collision.gameObject.tag == "Death")
        {
            Destroy(this.gameObject);
        }
    }
}
