﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffManager : MonoBehaviour
{
    public static bool activateBuff_1;
    public static bool activateBuff_2;
    public static bool activateBuff_3;

    public GameObject buffType_1;
    public GameObject buffType_2;
    public GameObject buffType_3;

    private void Start()
    {
        buffType_1.SetActive(false);
        buffType_2.SetActive(false);
        buffType_3.SetActive(false);
    }

    public void ShowBuffType(int type)
    {
        if(type == 1)
        {
            buffType_1.SetActive(true);
            buffType_2.SetActive(false);
            buffType_3.SetActive(false);

            buffType_1.gameObject.GetComponent<RectTransform>().SetAsLastSibling();
        }

        if (type == 2)
        {
            buffType_1.SetActive(false);
            buffType_2.SetActive(true);
            buffType_3.SetActive(false);

            buffType_2.gameObject.GetComponent<RectTransform>().SetAsLastSibling();
        }

        if (type == 3)
        {
            buffType_1.SetActive(false);
            buffType_2.SetActive(false);
            buffType_3.SetActive(true);

            buffType_3.gameObject.GetComponent<RectTransform>().SetAsLastSibling();
        }
    }
}
