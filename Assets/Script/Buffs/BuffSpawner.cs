﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSpawner : MonoBehaviour
{
    public static bool spawnBuff;

    public Buffs[] buffs;

    public Transform platformInstantiatePoint;
    public Transform spawnPoint;

    private void Update()
    {
        if (spawnBuff) 
        {
            InstantiateBuff();
        }
    }

    void InstantiateBuff()
    {
        float rndm = Random.value;

        if (rndm > 0.9f)
        {
            Buffs buff = Instantiate(buffs[0]);
            // -2.5 sampai 2.5
            float rndmNbr = Random.Range(-2.5f, 2.5f);
            spawnPoint.position = new Vector3(rndmNbr, platformInstantiatePoint.position.y + 0.65f);
            buff.transform.position = spawnPoint.position;
        }

        spawnBuff = false;
    }
}
