﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffType_1 : MonoBehaviour
{
    bool isNormal = true;
    float duration = 0;

    private void Update()
    {
        if (BuffManager.activateBuff_1 && duration > 0)
        {
            duration -= Time.deltaTime;
            Platforms[] thePlatforms = FindObjectsOfType<Platforms>();

            foreach (Platforms p in thePlatforms)
            {
                // p.transform.position = new Vector2(0, p.transform.position.y);
                // p.transform.position = Vector3.Lerp(p.transform.position, new Vector3(0, p.transform.position.y), Time.deltaTime / 0.5f);
                p.transform.position = Vector3.MoveTowards(p.transform.position, new Vector3(0, p.transform.position.y), 1);
            }

            isNormal = false;
        }

        else if (BuffManager.activateBuff_1 && duration <= 0)
        {
            if (!isNormal)
            {
                Platforms[] thePlatforms = FindObjectsOfType<Platforms>();

                foreach (Platforms p in thePlatforms)
                {
                    p.speed = Random.Range(1.8f, 2.3f);
                    p.speed += (p.speed * 0.5f * GameManager.cpCount);
                    p.RandomizeDir();
                }

                BuffManager.activateBuff_1 = false;
                isNormal = true;
            }
        }
    }

    public void ActivateBuff_1()
    {
        // Ketika ditekan buff aktif
        if (!BuffManager.activateBuff_1)
        {
            BuffManager.activateBuff_1 = true;
            duration = 5;
            FindObjectOfType<BuffManager>().buffType_1.SetActive(false);
        }
    }    
}
