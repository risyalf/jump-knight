﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffType_3 : MonoBehaviour
{
    PlayerControl playerControl;

    bool isNormal;
    float duration;

    private void Start()
    {
        playerControl = GameObject.Find("Player").GetComponent<PlayerControl>();
    }

    private void Update()
    {
        if (BuffManager.activateBuff_3 && duration > 0)
        {            
            duration -= Time.deltaTime;
            isNormal = false;
        }

        else if (BuffManager.activateBuff_3 && duration <= 0)
        {
            if (!isNormal)
            {
                playerControl.jumpSpeed = 16;
                playerControl.jumpLimit = 16;
                playerControl.sliderSpeed = 1;
                BuffManager.activateBuff_3 = false;
                isNormal = true;
            }
        }
    }

    public void ActiveBuff_3()
    {
        /*playerControl.jumpSpeed = 30;
        playerControl.jumpLimit = 30;
        playerControl.sliderSpeed = 1;*/

        playerControl.jumpSpeed = 25;
        playerControl.jumpLimit = 25;
        playerControl.sliderSpeed = 1;

        print("buff 3 activated");
        duration = 5;
        BuffManager.activateBuff_3 = true;
        FindObjectOfType<BuffManager>().buffType_3.SetActive(false);
    }
}
