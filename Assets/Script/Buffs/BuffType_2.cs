﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffType_2 : MonoBehaviour
{
    GameObject player;

    bool isNormal;
    float duration;

    private void Start()
    {
        player = GameObject.Find("Player");
    }

    private void Update()
    {
        if (BuffManager.activateBuff_2 && duration > 0)
        {
            player.transform.position = new Vector3(0, player.transform.position.y + (5 * Time.deltaTime));

            duration -= Time.deltaTime;
            isNormal = false;
        }

        else if (BuffManager.activateBuff_2 && duration <= 0)
        {
            if (!isNormal)
            {
                player.transform.position = player.transform.position;
                player.GetComponent<Rigidbody2D>().gravityScale = 3;
                player.GetComponent<Collider2D>().enabled = true;
                FindObjectOfType<JumpControl>().enabled = true;

                BuffManager.activateBuff_2 = false;
                isNormal = true;
            }
        }
    }

    public void ActiveBuff_2()
    {
        player.GetComponent<Rigidbody2D>().gravityScale = 0;
        player.GetComponent<Collider2D>().enabled = false;
        FindObjectOfType<JumpControl>().enabled = false;

        duration = 5;
        BuffManager.activateBuff_2 = true;
        FindObjectOfType<BuffManager>().buffType_2.SetActive(false);
    }

}
