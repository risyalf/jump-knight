﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindState : MonoBehaviour
{
    public static bool normal;
    public static bool blowsLeft;
    public static bool blowsRight;
    public static bool windy;               // sebagai patokan kapan ada angin
    public static bool windArea;            // sebagai patokan bila sudah berada di area berangin (point 20.000 keatas)

    public static float minimDur;
    public static float maximDur;

    static float randomizeState;    

    public float minDur;
    public float maxDur;

    private void Start()
    {
        normal = true;
        blowsLeft = false;
        blowsRight = false;
        windy = false;

        randomizeState = Random.Range(minDur, maxDur);

        minimDur = minDur;
        maximDur = maxDur;
    }

    private void Update()
    {
        if (windArea && randomizeState > 0)
        {
            randomizeState -= Time.deltaTime;
        }

        if (randomizeState < 0)
        {
            windy = true;
            randomizeState = 0;
        }

        if (windy)
        {
            StartCoroutine(WindGenerator());
        }
    }

    public static IEnumerator WindGenerator()
    {
        int randomize = Random.Range(0, 2);

        if (randomize == 0)
        {
            normal = false;
            blowsLeft = true;
            blowsRight = false;

            Debug.LogWarning("BIG WIND TO LEFT");
        }

        else if (randomize == 1)
        {
            normal = false;
            blowsLeft = false;
            blowsRight = true;

            Debug.LogWarning("BIG WIND TO RIGHT");
        }
        
        windy = false;

        yield return new WaitForSeconds(5);

        normal = true;
        blowsLeft = false;
        blowsRight = false;
        
        randomizeState = Random.Range(minimDur, maximDur);
    }
}
