﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudsManager : MonoBehaviour
{
    public GameObject cloudsHolder;
    // public GameObject cloudsSpawnPos;

    public GameObject[] cloudsLeft;
    public GameObject[] cloudsRight;

    public Transform leftSpawn;
    public Transform rightSpawn;
    public Transform playerPosition;

    bool spawnTheClouds;
    float spawnTimer;

    private void Start()
    {
        spawnTimer = 3;
    }

    private void Update()
    {
        if (ScoreManager.finalScore > 7000 && spawnTimer > 0)
        {
            spawnTimer -= Time.deltaTime;
        }

        if (spawnTimer < 0)
        {
            print("Spawn");
            spawnTheClouds = true;
            SpawnCloud();

            if (ScoreManager.finalScore > 5000 && ScoreManager.finalScore <= 10000)
            {
                spawnTimer = 15;
            }

            if (ScoreManager.finalScore > 10000 && ScoreManager.finalScore <= 15000)
            {
                spawnTimer = 10;
            }

            if (ScoreManager.finalScore > 15000)
            {
                spawnTimer = 7;
            }
        }

        leftSpawn.position = new Vector3(leftSpawn.position.x, playerPosition.position.y + 10, leftSpawn.position.z);
        rightSpawn.position = new Vector3(rightSpawn.position.x, playerPosition.position.y + 15, rightSpawn.position.z);
    }

    public void SpawnCloud()
    {
        if (spawnTheClouds)
        {
            int rndm = Random.Range(0, 2);

            GameObject leftCloud = Instantiate(cloudsLeft[rndm], cloudsHolder.transform);
            leftCloud.transform.position = leftSpawn.position;

            GameObject rightCloud = Instantiate(cloudsRight[rndm], cloudsHolder.transform);
            rightCloud.transform.position = rightSpawn.position;

            spawnTheClouds = false;
        }
    }
}
