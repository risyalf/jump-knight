﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clouds : MonoBehaviour
{
    public int dir;

    // int[] dir = new int[] { -1, 1 };
    // int rndm;

    private void Start()
    {
        // rndm = Random.Range(0, dir.Length);
    }

    private void Update()
    {
        if (WindState.normal)
        {
            transform.position = new Vector3(transform.position.x + (0.2f * Time.deltaTime * dir), transform.position.y, transform.position.z);
        }

        if (WindState.blowsLeft)
        {
            transform.position = new Vector3(transform.position.x - (0.5f * Time.deltaTime), transform.position.y, transform.position.z);
        }

        if (WindState.blowsRight)
        {
            transform.position = new Vector3(transform.position.x + (0.5f * Time.deltaTime), transform.position.y, transform.position.z);
        }
    }
}
