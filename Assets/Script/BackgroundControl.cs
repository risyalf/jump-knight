﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundControl : MonoBehaviour
{
    float posStart;

    private void Start()
    {
        posStart = transform.localPosition.y;
    }

    private void Update()
    {
        transform.localPosition = new Vector3(transform.localPosition.x, posStart - (ScoreManager.finalScore / 2000), transform.localPosition.z);
    }
}
