﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platforms : MonoBehaviour
{
    public float speed;

    public int type;
    [Range(0, 1)]
    public float sideLimit;
    public LayerMask layer;

    public bool movLeft;
    public bool movRight;

    bool collided;
    bool isDestroy;

    private void Start()
    {
        speed = Random.Range(1.8f, 2.3f);
        /*speed += (speed * 0.2f * GameManager.cpCount);*/
        speed += (speed * 0.2f + (0.25f * GameManager.cpCount));

        RandomizeDir();
    }

    private void FixedUpdate()
    {
        RaycastHit2D hitRight = Physics2D.Raycast(transform.position, Vector2.right, sideLimit, layer);
        RaycastHit2D hitLeft = Physics2D.Raycast(transform.position, Vector2.left, sideLimit, layer);
        RaycastHit2D hitDown = Physics2D.Raycast(transform.position, Vector2.down, sideLimit, layer);

        if (movRight)
        {
            if (WindState.normal)
                transform.position = new Vector3(transform.position.x + (Time.deltaTime * speed), transform.position.y);

            if (WindState.blowsLeft)
                transform.position = new Vector3(transform.position.x + ((Time.deltaTime * speed) * 0.5f), transform.position.y);

            if (WindState.blowsRight)
                transform.position = new Vector3(transform.position.x + ((Time.deltaTime * speed) * 1.5f), transform.position.y);
        }

        if (movLeft)
        {
            if (WindState.normal)
                transform.position = new Vector3(transform.position.x - (Time.deltaTime * speed), transform.position.y);

            if (WindState.blowsLeft)
                transform.position = new Vector3(transform.position.x - ((Time.deltaTime * speed) * 1.5f), transform.position.y);

            if (WindState.blowsRight)
                transform.position = new Vector3(transform.position.x - ((Time.deltaTime * speed) * 0.5f), transform.position.y);
        }

        if (hitRight.collider != null)
        {
            movLeft = true;
            movRight = false;
        }

        if (hitLeft.collider != null)
        {
            movLeft = false;
            movRight = true;
        }

        if (hitDown.collider != null)
        {
            print("hitDown");

            if (hitDown.collider.gameObject.name == "Death")
            {
                Destroy(this.gameObject);
            }
        }

        if (speed > 5)
        {
            speed = 5;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            StartCoroutine(ColliderDetect());
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collided = false;

            if (isDestroy)
            {
                StartCoroutine(DestroyDelay());
            }
        }
    }

    private IEnumerator ColliderDetect()
    {
        collided = true;

        yield return new WaitForSeconds(0.3f);

        if (collided)
        {
            isDestroy = true;

            if (type == 1)
            {
                speed *= 0.5f;
                print(speed);
            }

            else if (type == 2)
            {
                speed *= 0.75f;
                print(speed);
            }

            else if (type == 3)
            {
                speed *= 1.5f;
                print(speed);
            }
        }
    }

    private IEnumerator DestroyDelay()
    {
        GetComponent<Collider2D>().enabled = false;
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }

    public void RandomizeDir()
    {
        int rndm = Random.Range(0, 2);

        if (rndm == 0)
        {
            movRight = true;
            movLeft = false;
        }

        else
        {
            movRight = false;
            movLeft = true;
        }
    }
}
