﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformsManager : MonoBehaviour
{
    public static int platformTypeMax;
    public static int platformTypeMin;

    public List<GameObject> Platforms = new List<GameObject>();

    public Transform instantiatePoint;      // setiap playerPoint melewati mark, diinstantiate di instPoint, kemudian instpoint + 2
    public Transform instantiateMark;       // tanda bila player melewati ini maka akan diinstantiate pada instantiate point, kemudian + 2
    public Transform playerPoint;

    private void Start()
    {
        platformTypeMax = 3;
        platformTypeMin = 0;
    }

    private void Update()
    {
        if(playerPoint.position.y > instantiateMark.position.y)
        {
            // instantiate random dari platforms dengan posisi sama dengan point
            InstantiatePlatform(instantiatePoint);
            instantiateMark.position = new Vector3(instantiateMark.position.x, instantiateMark.position.y + 2);
            instantiatePoint.position = new Vector3(instantiatePoint.position.x, instantiatePoint.position.y + 2);
            BuffSpawner.spawnBuff = true;
        }
    }

    void InstantiatePlatform(Transform pos)
    {
        int rndm = Random.Range(platformTypeMin, platformTypeMax);
        GameObject platforms = GameObject.Find("PlatformHolder");
        GameObject platform = Instantiate(Platforms[rndm], platforms.transform);
        platform.SetActive(true);

        float randomizer = Random.Range(0.15f, 0.25f);
        platform.transform.localScale = new Vector3(randomizer, platform.transform.localScale.y, platform.transform.localScale.z);

        platform.transform.position = instantiatePoint.position;
    }
}
