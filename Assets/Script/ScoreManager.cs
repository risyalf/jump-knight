﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static float finalScore;
    public static int highScore;
    public static int point;

    public Transform player;
    public Text scoreText;

    float startPos;                             // Posisi awal
    float tempScore;
    float scoreNow;

    private void Start()
    {
        startPos = player.transform.position.y;
        finalScore = 0;

        /*if (!PlayerPrefs.HasKey("HighScore"))
        {
            PlayerPrefs.SetInt("HighScore", 0);
        }*/
    }

    private void Update()
    {
        tempScore = Mathf.Round(player.transform.position.y - startPos) * 100;

        /*if (tempScore > scoreNow)
        {
            scoreNow = tempScore;
        }*/

        if (tempScore > finalScore)
        {
            finalScore = tempScore;
        }

        // finalScore = scoreNow;
        scoreText.text = finalScore.ToString();
    }    

    public static void HighScore()
    {
        /*if(PlayerPrefs.GetInt("HighScore") < finalScore)
        {
            PlayerPrefs.SetInt("HighScore", (int)finalScore);            
        }*/

        // 1
        if (highScore < finalScore)
        {
            highScore = (int)finalScore;
        }
    }

    public static void SaveScores()
    {
        print(point + " + " + (int)finalScore);
        point += (int)finalScore;
        print("point = " + point);

        SaveSystem.SaveScores();
    }

    public static void LoadScores()
    {
        ScoresData scoreData = SaveSystem.LoadScores();

        highScore = scoreData.highScore;
        point = scoreData.point;
    }
}
