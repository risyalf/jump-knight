﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public bool movToPlayer;
    public float dist;

    Transform playerPos;

    private void Start()
    {
        playerPos = GameObject.Find("Player").GetComponent<Transform>();
    }

    private void Update()
    {
        if (movToPlayer)
        {
            // transform.position = new Vector3(transform.position.x, playerPos.position.y + dist, transform.position.z);
            transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, transform.position.z), new Vector3(transform.position.x, playerPos.position.y + dist, transform.position.z), Time.deltaTime/0.5f);
            //playerRecentPos = playerPos.position;
        }

        if (BuffManager.activateBuff_2 || BuffManager.activateBuff_3)
        {
            if (GetComponent<Camera>().orthographicSize < 8)
            {
                GetComponent<Camera>().orthographicSize += (3 * Time.deltaTime);
            }

            if (GetComponent<Camera>().orthographicSize > 8)
            {
                GetComponent<Camera>().orthographicSize = 8;
            }

            // transform.position = new Vector3(0, transform.position.y + (5 * Time.deltaTime), transform.position.z);

            if (BuffManager.activateBuff_2)
            {
                transform.position = Vector3.Lerp(new Vector3(transform.position.x, transform.position.y, transform.position.z), new Vector3(transform.position.x, playerPos.position.y - 4, transform.position.z), Time.deltaTime / 0.5f);
            }
        }

        else if(!BuffManager.activateBuff_2 && !BuffManager.activateBuff_3)
        {
            if (GetComponent<Camera>().orthographicSize > 6)
            {
                GetComponent<Camera>().orthographicSize -= (3 * Time.deltaTime);
            }

            if (GetComponent<Camera>().orthographicSize < 6)
            {
                GetComponent<Camera>().orthographicSize = 6;
            }
        }
    }
}
