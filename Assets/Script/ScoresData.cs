﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScoresData
{
    public int highScore;
    public int point;

    public ScoresData()
    {
        highScore = ScoreManager.highScore;
        point = ScoreManager.point;
    }
}
