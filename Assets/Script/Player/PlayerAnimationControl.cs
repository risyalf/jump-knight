﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationControl : MonoBehaviour
{
    public static Animator playerAnim;
    public static bool start;

    private void Start()
    {
        playerAnim = GetComponent<Animator>();

        start = false;
        PlayerAnimation_Idle();
    }

    public static void PlayerAnimation_Idle()
    {
        playerAnim.Play("Knight_Idle");
    }

    public static void PlayerAnimation_Hold()
    {
        playerAnim.Play("Knight_jumphold");
    }

    public static void PlayerAnimation_JumpMidAir()
    {
        playerAnim.Play("Knight_jumpair");
    }

    public static void PlayerAnimation_Falldown()
    {
        if(start)
            playerAnim.Play("Knight_falldown");

        start = true;
    }
}
