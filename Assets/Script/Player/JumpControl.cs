﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JumpControl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        if (PlayerControl.isGround)
        {
            // Tipe 2
            /*if (GameManager.type_2)
            {
                PlayerControl.isJumping = true;
            }*/
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (PlayerControl.isGround)
        {
            print("down");
            FindObjectOfType<PlayerControl>().jumpPower = 0;
            PlayerControl.downMeter = false;
            PlayerControl.upMeter = true;

            PlayerAnimationControl.PlayerAnimation_Hold();
        }

        else if (!PlayerControl.isGround)
        {
            FindObjectOfType<PlayerControl>().jumpPower = 0;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (PlayerControl.isGround)
        {
            print("Up");
            PlayerControl.downMeter = false;
            PlayerControl.upMeter = false;
            PlayerControl.isJumping = true;

            PlayerAnimationControl.PlayerAnimation_JumpMidAir();
        }
    }
}
