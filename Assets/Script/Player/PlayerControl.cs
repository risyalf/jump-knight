﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    public static bool downMeter;
    public static bool upMeter;

    public static bool isGround;
    public static bool isJumping;
    public static bool hasJump;

    public float jumpSpeed;
    public float jumpPower;
    public float jumpLimit;
    public float jumpTimeCounter;
    public float afterJumpCounter;
    public float sliderSpeed;

    public Slider slider;

    Rigidbody2D myRigidBody;
    Collider2D myCollider;

    float posNow;
    float posCompare;

    private void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        myCollider = GetComponent<Collider2D>();

        downMeter = false;
        upMeter = false;

        jumpSpeed = 16;
        jumpLimit = 16;
        sliderSpeed = jumpLimit / jumpSpeed;

        jumpTimeCounter = 0.5f;
    }

    private void FixedUpdate()
    {
        if (upMeter)
        {
            jumpPower += (Time.deltaTime * jumpSpeed);
            Debug.DrawLine(transform.position, new Vector3(transform.position.x, transform.position.y + jumpPower), Color.black);
            /*slider.value += Time.deltaTime / (1.6f);*/
            slider.value += Time.deltaTime / (sliderSpeed);
            // Debug.DrawLine(transform.position, new Vector3(transform.position.x, transform.position.y + GetComponent<Rigidbody2D>().velocity.y), Color.black);            
        }

        if (downMeter)
        {
            jumpPower -= (Time.deltaTime * jumpSpeed);
            Debug.DrawLine(transform.position, new Vector3(transform.position.x, transform.position.y + jumpPower), Color.black);
            slider.value -= Time.deltaTime / (sliderSpeed);
            // Debug.DrawLine(transform.position, new Vector3(transform.position.x, transform.position.y + GetComponent<Rigidbody2D>().velocity.y), Color.black);
        }

        if (jumpPower >= jumpLimit && upMeter)
        {
            upMeter = false;
            downMeter = true;
        }

        if (jumpPower <= 0 && downMeter)
        {
            upMeter = true;
            downMeter = false;
        }

        if (isGround && isJumping)
        {
            myRigidBody.velocity = new Vector2(0, jumpPower);

            isJumping = false;
        }

        // Mematikan collider saat tidak berada dipuncak, mengaktifkan RB setelah berada dipuncak
        if (!isGround)
        {
            posNow = transform.position.y;

            if (posNow > posCompare)
            {
                posCompare = posNow;
                myCollider.enabled = false;
            }

            else
            {
                myCollider.enabled = true;

                PlayerAnimationControl.PlayerAnimation_Falldown();
            }
        }

        // TERPENGARUH PADA WIND STATE
        if(!isGround)
        {
            if (WindState.blowsLeft)
            {
                transform.position = new Vector3(transform.position.x - Time.deltaTime, transform.position.y);
            }

            if (WindState.blowsRight)
            {
                transform.position = new Vector3(transform.position.x + Time.deltaTime, transform.position.y);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            SuccessfullyLanding();
            transform.parent = collision.collider.gameObject.transform;
            myCollider.enabled = true;
        }

        if (collision.gameObject.name == "PlatformStart")
        {
            isGround = true;
        }

        if (collision.gameObject.tag == "Death")
        {
            print("You Dead");
            DeathPanelControl.isDeath = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform")
        {
            isGround = false;
            transform.parent = null;
            myCollider.enabled = true;
            FindObjectOfType<CameraControl>().movToPlayer = false;
        }
    }

    void SuccessfullyLanding()
    {
        isGround = true;
        slider.value = 0;
        FindObjectOfType<CameraControl>().movToPlayer = true;
        posCompare = posNow;

        // Dibawah Enumerator
        /*yield return new WaitForSeconds(0.25f);
        if (isGround)
        {
            FindObjectOfType<CameraMov>().movToPlayer = true;
            posCompare = posNow;
        }*/
        PlayerAnimationControl.PlayerAnimation_Idle();
    }
}
