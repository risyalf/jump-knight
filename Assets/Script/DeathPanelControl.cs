﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeathPanelControl : MonoBehaviour
{
    public static bool isDeath;

    GameObject deathPanel;
    Text finalScore;
    Text highScore;

    private void Start()
    {
        isDeath = false;
        deathPanel = GameObject.Find("DeathPanel");
        finalScore = GameObject.Find("FinalScore").GetComponent<Text>();
        highScore = GameObject.Find("HighScore").GetComponent<Text>();
        deathPanel.SetActive(false);
    }

    private void Update()
    {
        if (isDeath)
        {
            deathPanel.SetActive(true);
            finalScore.text = ScoreManager.finalScore.ToString();
            Time.timeScale = 0;

            ScoreManager.HighScore();
            ScoreManager.SaveScores();

            // highScore.text = PlayerPrefs.GetInt("HighScore").ToString();

            // 2
            highScore.text = ScoreManager.highScore.ToString();

            isDeath = false;
        }
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
