﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveSystem
{
    public static void SaveScores()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/game.data";
        FileStream stream = new FileStream(path, FileMode.Create);
        ScoresData scoresData = new ScoresData();

        formatter.Serialize(stream, scoresData);
        stream.Close();
    }

    public static ScoresData LoadScores()
    {
        string path = Application.persistentDataPath + "/game.data";

        if (File.Exists(path)) 
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            ScoresData scoresData = formatter.Deserialize(stream) as ScoresData;
            stream.Close();

            return scoresData;
        }

        else
        {
            Debug.LogError("Data is not found");
            return null;
        }
    }
}
