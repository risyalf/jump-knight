﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState : MonoBehaviour
{
    bool nextState;

    private void Start()
    {
        nextState = true;
    }

    private void Update()
    {
        if(ScoreManager.finalScore > 10000)
        {
            WindState.windArea = true;
        }

        if(nextState && ScoreManager.finalScore > 15000)
        {
            print("Change Platform");
            PlatformsManager.platformTypeMin = 3;
            PlatformsManager.platformTypeMax = 6;
            nextState = false;
        }
    }
}
