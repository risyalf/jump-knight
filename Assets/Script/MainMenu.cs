﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    // UI CONTROLLER

    // PANEL //
    // Option Panel

    // HighScore Panel
    public GameObject highScorePanel;
    public Text highScoreText;
    public Text pointText;

    // Credit Panel
    // Exit Panel

    private void Start()
    {
        // Panel
        highScorePanel.SetActive(false);

        highScoreText.text = ScoreManager.highScore.ToString();
        pointText.text = ScoreManager.point.ToString();
    }

    // BUTTON //
    // Play Button
    // Option Button
    // HighScore Button
    public void ShowHighScore()
    {
        highScorePanel.SetActive(true);
    }

    // Credit Button
    // Exit Button

    // Back Button
    public void BackToMainMenu()
    {
        highScorePanel.SetActive(false);
    }

}
