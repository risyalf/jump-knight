﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int cpCount;

    int checkPoint;

    private void Start()
    {
        ScoreManager.LoadScores();

        cpCount = 0;
        Time.timeScale = 1;
        checkPoint = 2500;
    }

    private void Update()
    {
        if(ScoreManager.finalScore > checkPoint)
        {
            cpCount++;
            checkPoint += 2500;
            Debug.LogWarning(cpCount);
        }
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
